"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://localhost:8080/classes/";
// biến toàn cục để lưu trữ id class đang dc update or delete. Mặc định = 0
var gClassId = 0;
//biến mảng hằng số chứa ds tên các thuộc tính
const gCLASS_COLS = ["id", "classCode", "className", "teacherName", "teacherPhone", "action"];
//biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gCLASS_ID_COL = 0;
const gCLASS_CODE_COL = 1;
const gCLASS_NAME_COL = 2;
const gTEACHER_NAME_COL = 3;
const gTEACHER_PHONE_COL = 4;
const gCLASS_ACTION_COL = 5;

//khai báo DataTable & mapping columns
var gClassTable = $("#class-table").DataTable({
  columns: [
    { data: gCLASS_COLS[gCLASS_ID_COL] },
    { data: gCLASS_COLS[gCLASS_CODE_COL] },
    { data: gCLASS_COLS[gCLASS_NAME_COL] },
    { data: gCLASS_COLS[gTEACHER_NAME_COL] },
    { data: gCLASS_COLS[gTEACHER_PHONE_COL] },
    { data: gCLASS_COLS[gCLASS_ACTION_COL] }
  ],
  columnDefs: [
    {
      targets: gCLASS_ACTION_COL,
      defaultContent: `
                    <img class="edit-class" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                    <img class="delete-class" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                `
    }
  ]
});
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
  onPageLoading();
});
//2 - C: gán sự kiện Create-thêm mới class
$("#btn-add-class").on("click", function () {
  onBtnAddNewClassClick();
});
//3 - U: gán sự kiện update-sửa 1 class
$("#class-table").on("click", ".edit-class", function () {
  onBtnEditClassClick(this);
});
//4 - D: gán sự kiện delete-xóa 1 class
$("#class-table").on("click", ".delete-class", function () {
  onBtnDeleteClassClick(this);
});

//gán sự kiện cho nút Create class(trên modal)
$("#btn-create-class").on("click", function () {
  onBtnCreateClassClick();
});
//gán sự kiện cho nút Update class(trên modal)
$("#btn-update-class").on("click", function () {
  onBtnUpdateClassClick();
});
//gán sự kiện cho nút Delete class(trên modal)
$("#btn-confirm-delete-class").on("click", function () {
  onBtnDeleteClassConfirmClick();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
  //1 - R: read/load class to table
  getAllClasss();
}
//hàm xử lý sự kiện nút Thêm mới
function onBtnAddNewClassClick() {
  //hiển thị modal trắng lên
  $("#create-class-modal").modal("show");
}
//hàm xử lý sự kiện Update class từ table
function onBtnEditClassClick(paramEdit) {
  //lưu thông tin classId đang dc edit vào biến toàn cục
  gClassId = getClassIdFromButton(paramEdit);
  console.log("Id của class tương ứng = " + gClassId);
  // load dữ liệu vào các trường dữ liệu trong modal
  getClassDataByClassId(gClassId);
}
//hàm xử lý sự kiện Delete class từ table
function onBtnDeleteClassClick(paramDelete) {
  //lưu thông tin classId đang dc xóa vào biến toàn cục
  gClassId = getClassIdFromButton(paramDelete);
  console.log("Id của class tương ứng = " + gClassId);
  $("#delete-class-modal").modal("show");
}

//hàm xử lý sự kiện nút Create class(trên modal)
function onBtnCreateClassClick() {
  //khai báo obj chứa class data
  var vRequestDataObj = {
    classCode: "",
    className: "",
    teacherName: "",
    teacherPhone: ""
  }
  //b1: get data
  getInsertClassData(vRequestDataObj);
  //b2: validate data
  var vIsValid = validateClassData(vRequestDataObj);
  //ghi ra console value trả ra của hàm validateclassData
  console.log(vIsValid);
  if (vIsValid) {
    //b3: insert class
    $.ajax({
      url: gBASE_URL,
      type: "POST",
      contentType: "application/json;charset-UTF-8",
      data: JSON.stringify(vRequestDataObj),
      success: function (paramRes) {
        console.log(paramRes);
        //b4: xử lý front-end
        handleInsertClassSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });
  }
}

//hàm xử lý sự kiện nút Update class(trên modal)
function onBtnUpdateClassClick() {
  //khai báo obj chứa class data
  var vRequestDataObj = {
    classCode: "",
    className: "",
    teacherName: "",
    teacherPhone: ""
  }
  //b1: get data
  getUpdateClassData(vRequestDataObj);
  //b2: validate data
  var vIsValid = validateClassData(vRequestDataObj);
  if (vIsValid) {
    //b3: update class
    $.ajax({
      url: gBASE_URL + gClassId,
      type: "PUT",
      contentType: "application/json;charset-UTF-8",
      data: JSON.stringify(vRequestDataObj),
      success: function (paramRes) {
        console.log(paramRes);
        //b4: xử lý front-end
        handleUpdateClassSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });
  }
}

// hàm xử lý sự kiện delete voucher modal click
function onBtnDeleteClassConfirmClick() {
  $.ajax({
    url: gBASE_URL + gClassId,
    type: "DELETE",
    contentType: "application/json;charset-UTF-8",
    success: function (paramRes) {
      // B4: xử lý front-end
      handleDeleteClassSuccess();
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load classs to DataTable
// in: classs array
// out: class table has data
function loadDataToTable(paramClass) {
  gClassTable.clear();
  gClassTable.rows.add(paramClass);
  gClassTable.draw();
}
//hàm gọi api để lấy all list class đăng ký
function getAllClasss() {
  $.ajax({
    url: gBASE_URL,
    type: "GET",
    success: function (paramClass) {
      //ghi response ra console
      console.log(paramClass);
      loadDataToTable(paramClass);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}
//hàm dựa vào button detail (edit or delete) xác định dc id class
function getClassIdFromButton(paramButton) {
  var vTableRow = $(paramButton).parents("tr");
  var vclassRowData = gClassTable.row(vTableRow).data();
  return vclassRowData.id;
}

//hàm thu thập insert data
function getInsertClassData(paramClassObj) {
  paramClassObj.classCode = $("#inp-create-class-code").val().trim();
  paramClassObj.className = $("#inp-create-class-name").val().trim();
  paramClassObj.teacherName = $("#inp-create-teacher-name").val().trim();
  paramClassObj.teacherPhone = $("#inp-create-teacher-phone").val().trim();
}
//hàm validate data
function validateClassData(paramClassObj) {
  if (paramClassObj.classCode === "") {
    alert("Class code is not blank!");
    return false;
  }
  if (paramClassObj.className === "") {
    alert("Class name is not blank!");
    return false;
  }
  if (paramClassObj.teacherName === "") {
    alert("Teacher name is not blank!");
    return false;
  }
  if (paramClassObj.teacherPhone === "") {
    alert("Teacher phone is not blank!");
    return false;
  }
  return true;
}
//hàm xử lý front-end khi thêm class thành công
function handleInsertClassSuccess() {
  alert("Add new class successfully!");
  getAllClasss();
  resetCreateClassForm();
  $("#create-class-modal").modal("hide");
}
//hàm xóa trắng form create class
function resetCreateClassForm() {
  $("#inp-create-class-code").val("");
  $("#inp-create-class-name").val("");
  $("#inp-create-teacher-name").val("");
  $("#inp-create-teacher-phone").val("");
}

//hàm load class data lên update modal form
function getClassDataByClassId(paramClassId) {
  $.ajax({
    url: gBASE_URL + paramClassId,
    type: "GET",
    success: function (paramClassId) {
      showClassDataToUpdateModal(paramClassId);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
  // hiển thị modal lên
  $("#update-class-modal").modal("show");
}
//hàm show class data lên update modal form
function showClassDataToUpdateModal(paramClassDataObj) {
  $("#inp-update-class-code").val(paramClassDataObj.classCode);
  $("#inp-update-class-name").val(paramClassDataObj.className);
  $("#inp-update-teacher-name").val(paramClassDataObj.teacherName);
  $("#inp-update-teacher-phone").val(paramClassDataObj.teacherPhone);
}
//hàm thu thập update data
function getUpdateClassData(paramClassObj) {
  paramClassObj.classCode = $("#inp-update-class-code").val().trim();
  paramClassObj.className = $("#inp-update-class-name").val().trim();
  paramClassObj.teacherName = $("#inp-update-teacher-name").val().trim();
  paramClassObj.teacherPhone = $("#inp-update-teacher-phone").val().trim();
}
//hàm xử lý front-end khi update class thành công
function handleUpdateClassSuccess() {
  alert("Update class successfully!");
  getAllClasss();
  $("#update-class-modal").modal("hide");
}
//hàm xử lý front-end khi delete class thành công
function handleDeleteClassSuccess() {
  alert("Delete class successfully!");
  getAllClasss();
  $("#delete-class-modal").modal("hide");
}