"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://localhost:8080/students/";
// biến toàn cục để lưu trữ id class đang dc update or delete. Mặc định = 0
var gStudentId = 0;
//biến mảng hằng số chứa ds tên các thuộc tính
const gSTUDENT_COLS = ["id", "studentCode", "studentName", "gender", "address", "studentPhone", "classes.className", "action"];
//biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gSTUDENT_ID_COL = 0;
const gSTUDENT_CODE_COL = 1;
const gSTUDENT_NAME_COL = 2;
const gSTUDENT_GENDER_COL = 3;
const gSTUDENT_ADDRESS_COL = 4;
const gSTUDENT_PHONE_COL = 5;
const gSTUDENT_CLASS_COL = 6;
const gSTUDENT_ACCTION_COL = 7;

//khai báo DataTable & mapping columns
var gStudentTable = $("#student-table").DataTable({
  columns: [
    { data: gSTUDENT_COLS[gSTUDENT_ID_COL] },
    { data: gSTUDENT_COLS[gSTUDENT_CODE_COL] },
    { data: gSTUDENT_COLS[gSTUDENT_NAME_COL] },
    { data: gSTUDENT_COLS[gSTUDENT_GENDER_COL] },
    { data: gSTUDENT_COLS[gSTUDENT_ADDRESS_COL] },
    { data: gSTUDENT_COLS[gSTUDENT_PHONE_COL] },
    { data: gSTUDENT_COLS[gSTUDENT_CLASS_COL] },
    { data: gSTUDENT_COLS[gSTUDENT_ACCTION_COL] }
  ],
  columnDefs: [
    {
      targets: gSTUDENT_ACCTION_COL,
      defaultContent: `
                    <img class="edit-student" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                    <img class="delete-student" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                `
    }
  ]
});
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
  onPageLoading();
});
//2 - C: gán sự kiện Create-thêm mới student
$("#btn-add-student").on("click", function () {
  onBtnAddNewStudentClick();
});
//3 - U: gán sự kiện update-sửa 1 student
$("#student-table").on("click", ".edit-student", function () {
  onBtnEditStudentClick(this);
});
//4 - D: gán sự kiện delete-xóa 1 student
$("#student-table").on("click", ".delete-student", function () {
  onBtnDeleteStudentClick(this);
});

//gán sự kiện cho nút Create student(trên modal)
$("#btn-create-student").on("click", function () {
  onBtnCreateStudentClick();
});
//gán sự kiện cho nút Update student(trên modal)
$("#btn-update-student").on("click", function () {
  onBtnUpdateStudentClick();
});
//gán sự kiện cho nút Delete student(trên modal)
$("#btn-confirm-delete-student").on("click", function () {
  onBtnDeleteStudentConfirmClick();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
  //1 - R: read/load student to table
  getAllStudents();
  getAllClass();
}
//hàm xử lý sự kiện nút Thêm mới
function onBtnAddNewStudentClick() {
  //hiển thị modal trắng lên
  $("#create-student-modal").modal("show");
}
//hàm xử lý sự kiện Update student từ table
function onBtnEditStudentClick(paramEdit) {
  //lưu thông tin studentId đang dc edit vào biến toàn cục
  gStudentId = getStudentIdFromButton(paramEdit);
  console.log("Id của student tương ứng = " + gStudentId);
  // load dữ liệu vào các trường dữ liệu trong modal
  getStudentDataByStudentId(gStudentId);
}
//hàm xử lý sự kiện Delete student từ table
function onBtnDeleteStudentClick(paramDelete) {
  //lưu thông tin studentId đang dc xóa vào biến toàn cục
  gStudentId = getStudentIdFromButton(paramDelete);
  console.log("Id của student tương ứng = " + gStudentId);
  $("#delete-student-modal").modal("show");
}

//hàm xử lý sự kiện nút Create student(trên modal)
function onBtnCreateStudentClick() {
  //khai báo obj chứa student data
  var vRequestDataObj = {
    studentCode: "",
    studentName: "",
    gender: "",
    address: "",
    studentPhone: "",
    classes: ""
  }
  //b1: get data
  getInsertStudentData(vRequestDataObj);
  //b2: validate data
  var vIsValid = validateStudentData(vRequestDataObj);
  //ghi ra console value trả ra của hàm validateStudentData
  console.log(vIsValid);
  if (vIsValid) {
    //b3: insert student
    $.ajax({
      url: gBASE_URL,
      type: "POST",
      contentType: "application/json;charset-UTF-8",
      data: JSON.stringify(vRequestDataObj),
      success: function (paramRes) {
        console.log(paramRes);
        //b4: xử lý front-end
        handleInsertStudentSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });
  }
}

//hàm xử lý sự kiện nút Update student(trên modal)
function onBtnUpdateStudentClick() {
  //khai báo obj chứa student data
  var vRequestDataObj = {
    studentCode: "",
    studentName: "",
    gender: "",
    address: "",
    studentPhone: "",
    classes: ""
  }
  //b1: get data
  getUpdateStudentData(vRequestDataObj);
  //b2: validate data
  var vIsValid = validateStudentData(vRequestDataObj);
  if (vIsValid) {
    //b3: update student
    $.ajax({
      url: gBASE_URL + gStudentId,
      type: "PUT",
      contentType: "application/json;charset-UTF-8",
      data: JSON.stringify(vRequestDataObj),
      success: function (paramRes) {
        console.log(paramRes);
        //b4: xử lý front-end
        handleUpdateStudentSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    });
  }
}

// hàm xử lý sự kiện delete student modal click
function onBtnDeleteStudentConfirmClick() {
  $.ajax({
    url: gBASE_URL + gStudentId,
    type: "DELETE",
    contentType: "application/json;charset-UTF-8",
    success: function (paramRes) {
      console.log(paramRes);
      // B4: xử lý front-end
      handleDeleteStudentSuccess();
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load student to DataTable
// in: student array
// out: student table has data
function loadDataToTable(paramStudent) {
  gStudentTable.clear();
  gStudentTable.rows.add(paramStudent);
  gStudentTable.draw();
}
//hàm gọi api để lấy all list console.log(paramRes); đăng ký
function getAllStudents() {
  $.ajax({
    url: gBASE_URL,
    type: "GET",
    success: function (paramStudent) {
      //ghi response ra console
      console.log(paramStudent);
      loadDataToTable(paramStudent);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}
//hàm gọi api load all class vào select
function getAllClass() {
  $.ajax({
    url: "http://localhost:8080/classes",
    method: 'GET',
    dataType: 'json',
    success: function (response) {
      var classCreateSelect = $('#select-create-class');
      $.each(response, function (index, classes) {
        var option = $('<option></option>').attr('value', classes.id).text(classes.className);
        classCreateSelect.append(option);
      });
      var classUpdateSelect = $('#select-update-class');
      $.each(response, function (index, classes) {
        var option = $('<option></option>').attr('value', classes.id).text(classes.className);
        classUpdateSelect.append(option);
      });
    },
    error: function (error) {
      console.log(error.responseText);
    }
  });
}
//hàm dựa vào button detail (edit or delete) xác định dc id student
function getStudentIdFromButton(paramButton) {
  var vTableRow = $(paramButton).parents("tr");
  var vStudentRowData = gStudentTable.row(vTableRow).data();
  return vStudentRowData.id;
}

//hàm thu thập insert data
function getInsertStudentData(paramStudentObj) {
  paramStudentObj.studentCode = $("#inp-create-student-code").val().trim();
  paramStudentObj.studentName = $("#inp-create-student-name").val().trim();
  paramStudentObj.gender = $("#select-create-gender").val();
  paramStudentObj.address = $("#inp-create-address").val().trim();
  paramStudentObj.studentPhone = $("#inp-create-phone").val().trim();
  var vClassId = $("#select-create-class").val();
  if (vClassId !== "") {
    paramStudentObj.classes = { id: parseInt(vClassId) };
  } else {
    paramStudentObj.classes = null;
  }
}
//hàm validate data
function validateStudentData(paramStudentObj) {
  if (!paramStudentObj.classes) {
    alert("Please choose class!");
    return false;
  }
  if (paramStudentObj.studentCode === "") {
    alert("Student code is not blank!");
    return false;
  }
  if (paramStudentObj.studentName === "") {
    alert("Student name is not blank!");
    return false;
  }
  if (paramStudentObj.gender === "") {
    alert("Please choose gender!");
    return false;
  }
  if (paramStudentObj.address === "") {
    alert("Address is not blank!");
    return false;
  }
  if (paramStudentObj.studentPhone === "") {
    alert("Phone is not blank!");
    return false;
  }
  return true;
}
//hàm xử lý front-end khi thêm student thành công
function handleInsertStudentSuccess() {
  alert("Add new student successfully!");
  getAllStudents();
  resetCreateStudentForm();
  $("#create-student-modal").modal("hide");
}
//hàm xóa trắng form create student
function resetCreateStudentForm() {
  $("#inp-create-student-code").val("");
  $("#inp-create-student-name").val("");
  $("#select-create-gender").val("");
  $("#inp-create-address").val("");
  $("#inp-create-phone").val("");
  $("#select-create-class").val("");
}

//hàm load student data lên update modal form
function getStudentDataByStudentId(paramStudentId) {
  $.ajax({
    url: gBASE_URL + paramStudentId,
    type: "GET",
    success: function (paramStudentId) {
      showStudentDataToUpdateModal(paramStudentId);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
  // hiển thị modal lên
  $("#update-student-modal").modal("show");
}
//hàm show student data lên update modal form
function showStudentDataToUpdateModal(paramStudentDataObj) {
  $("#inp-update-student-code").val(paramStudentDataObj.studentCode);
  $("#inp-update-student-name").val(paramStudentDataObj.studentName);
  $("#select-update-gender").val(paramStudentDataObj.gender);
  $("#inp-update-phone").val(paramStudentDataObj.studentPhone);
  $("#inp-update-address").val(paramStudentDataObj.address);
  $("#select-update-class").val(paramStudentDataObj.classes.id);
}
//hàm thu thập update data
function getUpdateStudentData(paramStudentObj) {
  paramStudentObj.studentCode = $("#inp-update-student-code").val().trim();
  paramStudentObj.studentName = $("#inp-update-student-name").val().trim();
  paramStudentObj.gender = $("#select-update-gender").val();
  paramStudentObj.address = $("#inp-update-address").val().trim();
  paramStudentObj.studentPhone = $("#inp-update-phone").val().trim();
  var vClassId = $("#select-update-class").val();
  if (vClassId !== "") {
    paramStudentObj.classes = { id: parseInt(vClassId) };
  } else {
    paramStudentObj.classes = null;
  }
}
//hàm xử lý front-end khi update student thành công
function handleUpdateStudentSuccess() {
  alert("Update student successfully!");
  getAllStudents();
  $("#update-student-modal").modal("hide");
}
//hàm xử lý front-end khi delete student thành công
function handleDeleteStudentSuccess() {
  alert("Delete student successfully!");
  getAllStudents();
  $("#delete-student-modal").modal("hide");
}