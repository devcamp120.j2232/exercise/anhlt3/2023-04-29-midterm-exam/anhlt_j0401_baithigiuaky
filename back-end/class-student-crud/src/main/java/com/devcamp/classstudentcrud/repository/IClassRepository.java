package com.devcamp.classstudentcrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.classstudentcrud.model.CClass;

public interface IClassRepository extends JpaRepository<CClass, Integer> {

}
