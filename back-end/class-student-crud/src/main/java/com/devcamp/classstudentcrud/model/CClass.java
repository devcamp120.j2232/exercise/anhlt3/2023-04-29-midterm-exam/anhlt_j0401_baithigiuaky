package com.devcamp.classstudentcrud.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "classes")
public class CClass {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotEmpty(message = "Nhập mã lớp")
    @Column(name = "class_code", unique = true)
    private String classCode;

    @NotEmpty(message = "Nhập tên lớp")
    @Column(name = "class_name")
    private String className;

    @NotEmpty(message = "Nhập tên GVCN")
    @Column(name = "teacher_name")
    private String teacherName;

    @NotEmpty(message = "Nhập sdt GVCN")
    @Column(name = "teacher_phone")
    private String teacherPhone;

    @OneToMany(mappedBy = "classes", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<CStudent> students;

    public CClass() {
    }

    public CClass(int id) {
        this.id = id;
    }
    
    public CClass(int id, String classCode, String className, String teacherName, String teacherPhone,
            Set<CStudent> students) {
        this.id = id;
        this.classCode = classCode;
        this.className = className;
        this.teacherName = teacherName;
        this.teacherPhone = teacherPhone;
        this.students = students;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getTeacherPhone() {
        return teacherPhone;
    }

    public void setTeacherPhone(String teacherPhone) {
        this.teacherPhone = teacherPhone;
    }

    public Set<CStudent> getStudents() {
        return students;
    }

    public void setStudents(Set<CStudent> students) {
        this.students = students;
    }

}
