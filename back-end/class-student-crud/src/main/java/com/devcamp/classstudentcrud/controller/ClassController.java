package com.devcamp.classstudentcrud.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.classstudentcrud.model.CClass;
import com.devcamp.classstudentcrud.repository.IClassRepository;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ClassController {
    @Autowired
	private IClassRepository pClassRepository;

    @PostMapping("/classes")
    public ResponseEntity<Object> createClass(@Valid @RequestBody CClass cClass) {
        try {
            CClass newRole = new CClass();
            newRole.setClassCode(cClass.getClassCode());
            newRole.setClassName(cClass.getClassName());
            newRole.setTeacherName(cClass.getTeacherName());
            newRole.setTeacherPhone(cClass.getTeacherPhone());
            newRole.setStudents(cClass.getStudents());
            CClass savedRole = pClassRepository.save(newRole);
            return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Class: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/classes/{id}")
    public ResponseEntity<Object> updateClassById(@PathVariable("id") Integer id, @Valid @RequestBody CClass cClass) {
        Optional<CClass> classData = pClassRepository.findById(id);
        if (classData.isPresent()) {
            CClass newClass = classData.get();
            newClass.setClassCode(cClass.getClassCode());
            newClass.setClassName(cClass.getClassName());
            newClass.setTeacherName(cClass.getTeacherName());
            newClass.setTeacherPhone(cClass.getTeacherPhone());
            newClass.setStudents(cClass.getStudents());
            CClass savedClass = pClassRepository.save(newClass);
            return new ResponseEntity<>(savedClass, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/classes/{id}")
    public ResponseEntity<Object> deleteClassById(@PathVariable Integer id) {
        try {
            Optional<CClass> optional = pClassRepository.findById(id);
            if (optional.isPresent()) {
                pClassRepository.deleteById(id);
            }
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/classes")
    public ResponseEntity<List<CClass>> getAllClasses() {
        try {
            List<CClass> pClasses = new ArrayList<CClass>();

            pClassRepository.findAll().forEach(pClasses::add);

            return new ResponseEntity<>(pClasses, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/classes/{id}")
    public CClass getClassById(@PathVariable Integer id) {
        if (pClassRepository.findById(id).isPresent()) {
            return pClassRepository.findById(id).get();
        } else {
            return null;
        }
    }
}
