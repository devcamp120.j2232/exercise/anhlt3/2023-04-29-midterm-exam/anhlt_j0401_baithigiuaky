package com.devcamp.classstudentcrud.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.classstudentcrud.model.CClass;
import com.devcamp.classstudentcrud.model.CStudent;
import com.devcamp.classstudentcrud.repository.IClassRepository;
import com.devcamp.classstudentcrud.repository.IStudentRepository;

@RestController
@RequestMapping("/")
@CrossOrigin
public class StudentController {
    @Autowired
    private IClassRepository pClassRepository;

    @Autowired
    private IStudentRepository pStudentRepository;

    @PostMapping("/students")
    public ResponseEntity<Object> createStudent(@Valid @RequestBody CStudent cStudent) {
        try {
            CClass cClass = null;
            if (cStudent.getClasses() != null && !Objects.equals(cStudent.getClasses().getId(), null)) {
                cClass = pClassRepository.findById(cStudent.getClasses().getId())
                        .orElseThrow(() -> new IllegalArgumentException("Invalid class id"));
            }
            CStudent newStudent = new CStudent();
            newStudent.setStudentCode(cStudent.getStudentCode());
            newStudent.setStudentName(cStudent.getStudentName());
            newStudent.setGender(cStudent.getGender());
            newStudent.setAddress(cStudent.getAddress());
            newStudent.setStudentPhone(cStudent.getStudentPhone());
            newStudent.setClasses(cClass);
            CStudent savedRole = pStudentRepository.save(newStudent);
            return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Student: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/students/{id}")
    public ResponseEntity<Object> updateStudentById(@PathVariable("id") Integer id,
            @Valid @RequestBody CStudent cStudent) {
        Optional<CStudent> studentData = pStudentRepository.findById(id);
        if (studentData.isPresent()) {
            CClass cClass = null;
            if (cStudent.getClasses() != null && !Objects.equals(cStudent.getClasses().getId(), null)) {
                cClass = pClassRepository.findById(cStudent.getClasses().getId())
                        .orElseThrow(() -> new IllegalArgumentException("Invalid class id"));
            }
            CStudent newStudent = studentData.get();
            newStudent.setStudentCode(cStudent.getStudentCode());
            newStudent.setStudentName(cStudent.getStudentName());
            newStudent.setGender(cStudent.getGender());
            newStudent.setAddress(cStudent.getAddress());
            newStudent.setStudentPhone(cStudent.getStudentPhone());
            newStudent.setClasses(cClass);
            CStudent savedStudent = pStudentRepository.save(newStudent);
            return new ResponseEntity<>(savedStudent, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/students/{id}")
    public ResponseEntity<Object> deleteStudentById(@PathVariable Integer id) {
        try {
            Optional<CStudent> optional = pStudentRepository.findById(id);
            if (optional.isPresent()) {
                pStudentRepository.deleteById(id);
            }
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/students")
    public ResponseEntity<List<CStudent>> getAllStudents() {
        try {
            List<CStudent> pStudents = new ArrayList<CStudent>();

            pStudentRepository.findAll().forEach(pStudents::add);

            return new ResponseEntity<>(pStudents, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/students/{id}")
    public CStudent getStudentById(@PathVariable Integer id) {
        if (pStudentRepository.findById(id).isPresent()) {
            return pStudentRepository.findById(id).get();
        } else {
            return null;
        }
    }
}
