package com.devcamp.classstudentcrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.classstudentcrud.model.CStudent;

public interface IStudentRepository extends JpaRepository<CStudent, Integer> {

}
