package com.devcamp.classstudentcrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClassStudentCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClassStudentCrudApplication.class, args);
	}

}
