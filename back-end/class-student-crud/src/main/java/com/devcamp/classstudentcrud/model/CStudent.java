package com.devcamp.classstudentcrud.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "students")
public class CStudent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotEmpty(message = "Nhập mã học sinh")
    @Column(name = "student_code", unique = true)
    private String studentCode;

    @NotEmpty(message = "Nhập tên học sinh")
    @Column(name = "student_name")
    private String studentName;

    @NotEmpty(message = "Nhập giới tính")
    @Column(name = "gender")
    private String gender;

    @NotEmpty(message = "Nhập địa chỉ")
    @Column(name = "address")
    private String address;

    @NotEmpty(message = "Nhập số điện thoại liên hệ")
    @Column(name = "student_phone", unique = true)
    private String studentPhone;

    @ManyToOne
    @JoinColumn(name = "class_id")
    private CClass classes;

    public CStudent() {
    }

    public CStudent(int id, String studentCode, String studentName, String gender, String address, String studentPhone,
            CClass classes) {
        this.id = id;
        this.studentCode = studentCode;
        this.studentName = studentName;
        this.gender = gender;
        this.address = address;
        this.studentPhone = studentPhone;
        this.classes = classes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStudentCode() {
        return studentCode;
    }

    public void setStudentCode(String studentCode) {
        this.studentCode = studentCode;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStudentPhone() {
        return studentPhone;
    }

    public void setStudentPhone(String studentPhone) {
        this.studentPhone = studentPhone;
    }

    public CClass getClasses() {
        return classes;
    }

    public void setClasses(CClass classes) {
        this.classes = classes;
    }

}
