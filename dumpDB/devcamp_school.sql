INSERT INTO `classes` (`id`, `class_code`, `class_name`, `teacher_name`, `teacher_phone`) VALUES
(1, 'A1', 'className 1', 'teacher 1', '012345789'),
(2, 'A2', 'className 2', 'teacher 2', '012345781'),
(3, 'A3', 'className 3', 'teacher 3', '012345782'),
(4, 'A4', 'className 4', 'teacher 4', '012345783'),
(5, 'B1', 'className 5', 'teacher 5', '012345784'),
(6, 'B2', 'className 6', 'teacher 6', '012345785'),
(7, 'B3', 'className 7', 'teacher 7', '012345786'),
(8, 'C1', 'className 8', 'teacher 8', '012345787'),
(9, 'C2', 'className 9', 'teacher 9', '012345788'),
(10, 'C3', 'className 10', 'teacher 10', '012345723');

INSERT INTO
  `students`(`id`,`address`,`gender`,`student_code`,`student_name`,`student_phone`,`class_id`)
VALUES
  ('1','address 1','m','scode 1','name 1','0771234650','1'),
  ('2','address 2','m','scode 2','name 2','0771234651','1'),
  ('3','address 3','f','scode 3','name 3','0771234652','1'),
  ('4','address 4','m','scode 4','name 4','0771234653','1'),
  ('5','address 5','f','scode 5','name 5','0771234654','1'),
  ('6','address 6','f','scode 6','name 6','0771234655','1'),
  ('7','address 7','m','scode 7','name 7','0771234656','1'),
  ('8','address 8','f','scode 8','name 8','0771234657','1'),
  ('9','address 9','m','scode 9','name 9','0771234658','1'),

  ('10','address 10','m','scode 10','name 10','0771234659','1'),
  ('11','address 11','m','scode 11','name 11','0771234610','2'),
  ('12','address 12','f','scode 12','name 12','0771234621','2'),
  ('13','address 13','f','scode 13','name 13','0771234632','2'),
  ('14','address 14','f','scode 14','name 14','0771234643','2'),
  ('15','address 15','f','scode 15','name 15','0771234664','2'),
  ('16','address 16','f','scode 16','name 16','0771234455','2'),
  ('17','address 17','m','scode 17','name 17','0771234666','2'),
  ('18','address 18','f','scode 18','name 18','07712347677','2'),
  ('19','address 19','m','scode 19','name 19','0771234123','2'),
  ('20','address 20','m','scode 20','name 20','0771234310','2'),

  ('21','address 21','f','scode 21','name 21','0271234610','3'),
  ('22','address 22','f','scode 22','name 22','0271234621','3'),
  ('23','address 23','f','scode 23','name 23','0271234632','3'),
  ('24','address 24','m','scode 24','name 24','0271234643','3'),
  ('25','address 25','f','scode 25','name 25','02271234664','3'),
  ('26','address 26','m','scode 26','name 26','0271234455','3'),
  ('27','address 27','m','scode 27','name 27','0271234666','3'),
  ('28','address 28','f','scode 28','name 28','02712347677','3'),
  ('29','address 29','m','scode 29','name 29','0271234659','3'),
  ('30','address 30','m','scode 30','name 30','0271234310','3'),

  ('31','address 31','f','scode 31','name 31','0371234610','4'),
  ('32','address 32','m','scode 32','name 32','0371234621','4'),
  ('33','address 33','f','scode 33','name 33','0371234632','4'),
  ('34','address 34','m','scode 34','name 34','0371234643','4'),
  ('35','address 35','f','scode 35','name 35','0327123466','4'),
  ('36','address 36','f','scode 36','name 36','0371234455','4'),
  ('37','address 37','m','scode 37','name 37','0371234666','4'),
  ('38','address 38','f','scode 38','name 38','0371234767','4'),
  ('39','address 39','m','scode 39','name 39','0371234659','4'),
  ('40','address 40','m','scode 40','name 40','0371234310','4'),

  ('41','address 41','m','scode 41','name 41','0471234610','5'),
  ('42','address 42','m','scode 42','name 42','0471234621','5'),
  ('43','address 43','f','scode 43','name 43','0471234632','5'),
  ('44','address 44','m','scode 44','name 44','0471234643','5'),
  ('45','address 45','f','scode 45','name 45','0427123466','5'),
  ('46','address 46','m','scode 46','name 46','0471234455','5'),
  ('47','address 47','m','scode 47','name 47','0471234666','5'),
  ('48','address 48','f','scode 48','name 48','0471234767','5'),
  ('49','address 49','m','scode 49','name 49','0471234659','5'),
  ('50','address 50','m','scode 50','name 50','0471234310','5'),

  ('51','address 51','m','scode 51','name 51','0571234610','6'),
  ('52','address 52','m','scode 52','name 52','0571234621','6'),
  ('53','address 53','m','scode 53','name 53','0571234632','6'),
  ('54','address 54','m','scode 54','name 54','0571234643','6'),
  ('55','address 55','f','scode 55','name 55','0527123466','6'),
  ('56','address 56','m','scode 56','name 56','0571234455','6'),
  ('57','address 57','m','scode 57','name 57','0571234666','6'),
  ('58','address 58','f','scode 58','name 58','0571234767','6'),
  ('59','address 59','m','scode 59','name 59','0571234659','6'),
  ('60','address 50','f','scode 60','name 60','0571234310','6'),

  ('61','address 61','f','scode 61','name 61','0671234610','7'),
  ('62','address 62','m','scode 62','name 62','0671234621','7'),
  ('63','address 63','f','scode 63','name 63','0671234632','7'),
  ('64','address 64','m','scode 64','name 64','0671234643','7'),
  ('65','address 65','f','scode 65','name 65','0627123466','7'),
  ('66','address 66','m','scode 66','name 66','0671234455','7'),
  ('67','address 67','m','scode 67','name 67','0671234666','7'),
  ('68','address 68','f','scode 68','name 68','0671234767','7'),
  ('69','address 69','m','scode 69','name 69','0671234659','7'),
  ('70','address 70','f','scode 70','name 70','0671234310','7'),

  ('71','address 71','m','scode 71','name 71','0871234610','8'),
  ('72','address 72','m','scode 72','name 72','0871234621','8'),
  ('73','address 73','f','scode 73','name 73','0871234632','8'),
  ('74','address 74','m','scode 74','name 74','0871234643','8'),
  ('75','address 75','m','scode 75','name 75','0827123466','8'),
  ('76','address 76','m','scode 76','name 76','0871234455','8'),
  ('77','address 77','m','scode 77','name 77','0871234666','8'),
  ('78','address 78','f','scode 78','name 78','0871234767','8'),
  ('79','address 79','m','scode 79','name 79','0871234659','8'),
  ('80','address 80','f','scode 80','name 80','0871234310','8'),

  ('81','address 81','f','scode 81','name 81','0971234610','9'),
  ('82','address 82','f','scode 82','name 82','0971234621','9'),
  ('83','address 83','f','scode 83','name 83','0971234632','9'),
  ('84','address 84','m','scode 84','name 84','0971234643','9'),
  ('85','address 85','m','scode 85','name 85','0927123466','9'),
  ('86','address 86','f','scode 86','name 86','0971234455','9'),
  ('87','address 87','m','scode 87','name 87','0971234666','9'),
  ('88','address 88','f','scode 88','name 88','0971234767','9'),
  ('89','address 89','f','scode 89','name 89','0971234659','9'),
  ('90','address 90','f','scode 90','name 90','0971234310','9'),

  ('91','address 91','f','scode 91','name 91','0171234610','10'),
  ('92','address 92','m','scode 92','name 92','0171234621','10'),
  ('93','address 93','f','scode 93','name 93','0171234632','10'),
  ('94','address 94','m','scode 94','name 94','0171234643','10'),
  ('95','address 95','m','scode 95','name 95','0127123466','10'),
  ('96','address 96','f','scode 96','name 96','0171234455','10'),
  ('97','address 97','m','scode 97','name 97','0171234666','10'),
  ('98','address 98','f','scode 98','name 98','0171234767','10'),
  ('99','address 99','m','scode 99','name 99','0171234659','10'),
  ('100','address 100','f','scode 100','name 100','0171234310','10');